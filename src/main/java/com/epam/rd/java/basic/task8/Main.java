package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flowers;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length == 0) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);

		String xsdFileName = null;
		if (args.length > 1) {
			xsdFileName = args[1];
			System.out.println("XSD ==> " + xsdFileName);
		}
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////

		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		Flowers flowersDOM = domController.parseFromXML();

		// sort (case 1)
		// PLACE YOUR CODE HERE
		flowersDOM.sortByFlowerName();
		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		domController.parseToXML(flowersDOM, outputXmlFile);
		boolean isValidDOM = validateXML(outputXmlFile, xsdFileName);
		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		Flowers flowersSAX = saxController.parseFromXML();
		
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		flowersSAX.sortByFlowerOrigin();
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		saxController.parseToXML(flowersSAX, outputXmlFile);
		boolean isValidSAX = validateXML(outputXmlFile, xsdFileName);
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////

		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		Flowers flowersSTAX = staxController.parseFromXML();
		
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		flowersSTAX.sortByAveLenFlower();
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		staxController.parseToXML(flowersSTAX, outputXmlFile);
		boolean isValidStAX = validateXML(outputXmlFile, xsdFileName);
	}

	private static boolean validateXML(String xmlFileName, String xsdFileName) {
		if (xsdFileName == null) {
			return false;
		}

		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		try {
			Schema schema = factory.newSchema(new File(xsdFileName));
			Validator validator = schema.newValidator();
			validator.validate(new StreamSource(new File(xmlFileName)));
		} catch (SAXException | IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
