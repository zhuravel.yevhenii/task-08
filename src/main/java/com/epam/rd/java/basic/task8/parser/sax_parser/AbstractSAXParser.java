package com.epam.rd.java.basic.task8.parser.sax_parser;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

public abstract class AbstractSAXParser {

    protected final XMLStreamWriter writer;

    public AbstractSAXParser(XMLStreamWriter writer) {
        this.writer = writer;
    }

    protected void writeEndElement() throws XMLStreamException {
        writer.writeEndElement();
    }

    protected void writeCharacters(int value) throws XMLStreamException {
        writer.writeCharacters(String.valueOf(value));
    }

    protected void writeCharacters(String value) throws XMLStreamException {
        writer.writeCharacters(value);
    }

    protected void writeAttribute(String attrMeasure, String measure) throws XMLStreamException {
        writer.writeAttribute(attrMeasure, measure);
    }

    protected void writeStartElement(String nodeName) throws XMLStreamException {
        writer.writeStartElement(nodeName);
    }
}
