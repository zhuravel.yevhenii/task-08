package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.exception.XMLParserException;
import com.epam.rd.java.basic.task8.parser.dom_parser.FlowersDOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Controller for DOM parser.
 */
public class DOMController implements XMLController {

    private final String xmlFileName;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    @Override
    public Flowers parseFromXML() throws XMLParserException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Flowers flowers;

        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document document = db.parse(new File(xmlFileName));
            document.getDocumentElement().normalize();

            flowers = FlowersDOMParser.parseFromXML(document.getDocumentElement());
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
            throw new XMLParserException("DOM parse from XML error", e);
        }
        return flowers;
    }

    @Override
    public void parseToXML(Flowers flowers, String outputXmlFile) throws XMLParserException {
        try (FileOutputStream outputStream = new FileOutputStream(outputXmlFile)) {
            Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();

            Element rootElement = FlowersDOMParser.parseToXML(document, flowers);
            document.appendChild(rootElement);

            writeXML(document, outputStream);

        } catch (IOException | ParserConfigurationException e) {
            e.printStackTrace();
            throw new XMLParserException("DOM parse to XML error", e);
        }
    }

    private void writeXML(Document document, FileOutputStream outputStream) throws XMLParserException {
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(outputStream);

            transformer.transform(source, result);
        } catch (TransformerException e) {
            e.printStackTrace();
            throw new XMLParserException("DOM write to XML error", e);
        }
    }
}
