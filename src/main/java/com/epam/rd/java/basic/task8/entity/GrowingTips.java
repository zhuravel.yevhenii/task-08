package com.epam.rd.java.basic.task8.entity;

public class GrowingTips {

    private Temperature temperature;
    private String lighting;
    private Watering watering;

    public GrowingTips() {
    }

    public GrowingTips(Temperature temperature, String lighting, Watering watering) {
        this.temperature = temperature;
        this.lighting = lighting;
        this.watering = watering;
    }

    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    public String getLighting() {
        return lighting;
    }

    public void setLighting(String lighting) {
        this.lighting = lighting;
    }

    public Watering getWatering() {
        return watering;
    }

    public void setWatering(Watering watering) {
        this.watering = watering;
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "temperature=" + temperature +
                ",\n lighting='" + lighting + "'" +
                ",\n watering=" + watering +
                '}';
    }
}
