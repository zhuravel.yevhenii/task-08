package com.epam.rd.java.basic.task8.parser.constants;

public interface GrowingTipsFields {
    String NODE_NAME = "growingTips";
    String ELEMENT_LIGHTING = "lighting";
    String ATTR_LIGHT_REQUIRING = "lightRequiring";
}
