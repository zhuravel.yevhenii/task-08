package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.exception.XMLParserException;

public interface XMLController {
    Flowers parseFromXML() throws XMLParserException;
    void parseToXML(Flowers flowers, String outputXmlFile) throws XMLParserException;
}
