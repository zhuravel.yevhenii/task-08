package com.epam.rd.java.basic.task8.parser.dom_parser;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.parser.constants.FlowerFields;
import com.epam.rd.java.basic.task8.parser.constants.FlowersFields;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.Map;

public class FlowersDOMParser {

    public static Flowers parseFromXML(Element rootNode) {
        Flowers flowers = new Flowers();

        for (int i = 0; i < rootNode.getAttributes().getLength(); i++) {
            Node node = rootNode.getAttributes().item(i);
            flowers.addAttribute(node.getNodeName(), node.getTextContent());
        }

        NodeList flowerNodes = rootNode.getElementsByTagName(FlowerFields.NODE_NAME);
        for (int i = 0; i < flowerNodes.getLength(); i++) {
            Element element = (Element) flowerNodes.item(i);

            Flower flower = FlowerDOMParser.parseFromXML(element);

            flowers.addFlower(flower);
        }
        return flowers;
    }

    public static Element parseToXML(Document document, Flowers flowers) {
        Element rootElement = document.createElement(FlowersFields.NODE_NAME);

        for(Map.Entry<String, String> entry: flowers.getAllAttributes().entrySet()) {
            rootElement.setAttribute(entry.getKey(), entry.getValue());
        }

        for (Flower flower: flowers.getFlowers()) {
            rootElement.appendChild(FlowerDOMParser.parseToXML(document, flower));
        }

        return rootElement;
    }
}
