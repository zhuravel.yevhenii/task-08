package com.epam.rd.java.basic.task8.parser.stax_parser;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;

public abstract class AbstractStAXParser {

    protected final XMLEventWriter writer;
    protected final XMLEventFactory eventFactory;

    public AbstractStAXParser(XMLEventWriter writer, XMLEventFactory eventFactory) {
        this.writer = writer;
        this.eventFactory = eventFactory;
    }

    protected void createEndElement(String nodeName) throws XMLStreamException {
        writer.add(eventFactory.createEndElement("", "", nodeName));
    }

    protected void createCharacters(int value) throws XMLStreamException {
        writer.add(eventFactory.createCharacters(String.valueOf(value)));
    }

    protected void createCharacters(String value) throws XMLStreamException {
        writer.add(eventFactory.createCharacters(value));
    }

    protected void createAttribute(String attrMeasure, String measure) throws XMLStreamException {
        writer.add(eventFactory.createAttribute(attrMeasure, measure));
    }

    protected void createStartElement(String nodeName) throws XMLStreamException {
        writer.add(eventFactory.createStartElement("", "", nodeName));
    }
}
