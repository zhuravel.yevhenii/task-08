package com.epam.rd.java.basic.task8.parser.dom_parser;

import com.epam.rd.java.basic.task8.entity.Watering;
import com.epam.rd.java.basic.task8.parser.constants.WateringFields;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class WateringDOMParser {

    public static Watering parseFromXML(Element growingTips) {
        Node node = growingTips.getElementsByTagName(WateringFields.NODE_NAME).item(0);

        String measure = node.getAttributes().getNamedItem(WateringFields.ATTR_MEASURE).getTextContent();
        int value = Integer.parseInt(node.getTextContent());

        return new Watering(measure, value);
    }

    public static Element parseToXML(Document document, Watering watering) {
        Element visualParametersElement = document.createElement(WateringFields.NODE_NAME);
        visualParametersElement.setAttribute(WateringFields.ATTR_MEASURE, watering.getMeasure());
        visualParametersElement.setTextContent(String.valueOf(watering.getValue()));

        return visualParametersElement;
    }
}
