package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.AveLenFlower;
import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.Temperature;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import com.epam.rd.java.basic.task8.entity.Watering;
import com.epam.rd.java.basic.task8.exception.XMLParserException;
import com.epam.rd.java.basic.task8.parser.constants.AveLenFlowerFields;
import com.epam.rd.java.basic.task8.parser.constants.FlowerFields;
import com.epam.rd.java.basic.task8.parser.constants.FlowersFields;
import com.epam.rd.java.basic.task8.parser.constants.GrowingTipsFields;
import com.epam.rd.java.basic.task8.parser.constants.TemperatureFields;
import com.epam.rd.java.basic.task8.parser.constants.VisualParametersFields;
import com.epam.rd.java.basic.task8.parser.constants.WateringFields;
import com.epam.rd.java.basic.task8.parser.sax_parser.CustomSAXParser;
import com.epam.rd.java.basic.task8.parser.sax_parser.FlowersSAXParser;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler implements XMLController {
	
	private final String xmlFileName;
	private Flowers flowers;
	private Flower flower;

	private StringBuilder currentValue = new StringBuilder();

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	@Override
	public Flowers parseFromXML() throws XMLParserException {
		flowers = new Flowers();

		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {
			SAXParser parser = factory.newSAXParser();

			parser.parse(xmlFileName, this);

		} catch (ParserConfigurationException | IOException | SAXException e) {
			e.printStackTrace();
			throw new XMLParserException("SAX parse from XML error", e);
		}

		return flowers;
	}

	@Override
	public void parseToXML(Flowers flowers, String outputXmlFile) throws XMLParserException {
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		try {
			XMLStreamWriter writer = factory.createXMLStreamWriter(new FileOutputStream(outputXmlFile));

			CustomSAXParser<Flowers> parser = new FlowersSAXParser(writer);
			parser.toXML(flowers);

			writer.writeEndDocument();
			writer.flush();
			writer.close();
		} catch (XMLStreamException | FileNotFoundException e) {
			e.printStackTrace();
			throw new XMLParserException("SAX parse to XML error", e);
		}
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) {
		currentValue.setLength(0);

		if (qName.equalsIgnoreCase(FlowersFields.NODE_NAME)) {
			for (int i = 0; i < attributes.getLength(); i++) {
				flowers.addAttribute(attributes.getQName(i), attributes.getValue(i));
			}
		} else if (qName.equalsIgnoreCase(FlowerFields.NODE_NAME)) {
			flower = new Flower();

		} else if (qName.equalsIgnoreCase(VisualParametersFields.NODE_NAME)) {
			flower.setVisualParameters(new VisualParameters());

		} else if (qName.equalsIgnoreCase(AveLenFlowerFields.NODE_NAME)) {
			AveLenFlower aveLenFlower = new AveLenFlower();
			aveLenFlower.setMeasure(attributes.getValue(AveLenFlowerFields.ATTR_MEASURE));
			flower.getVisualParameters().setAveLenFlower(aveLenFlower);

		} else if (qName.equalsIgnoreCase(GrowingTipsFields.NODE_NAME)) {
			flower.setGrowingTips(new GrowingTips());

		} else if (qName.equalsIgnoreCase(TemperatureFields.NODE_NAME)) {
			Temperature temperature = new Temperature();
			temperature.setMeasure(attributes.getValue(TemperatureFields.ATTR_MEASURE));
			flower.getGrowingTips().setTemperature(temperature);

		} else if (qName.equalsIgnoreCase(GrowingTipsFields.ELEMENT_LIGHTING)) {
			flower.getGrowingTips().setLighting(attributes.getValue(GrowingTipsFields.ATTR_LIGHT_REQUIRING));

		} else if (qName.equalsIgnoreCase(WateringFields.NODE_NAME)) {
			Watering watering = new Watering();
			watering.setMeasure(attributes.getValue(WateringFields.ATTR_MEASURE));
			flower.getGrowingTips().setWatering(watering);
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) {
		if (qName.equalsIgnoreCase(FlowerFields.NODE_NAME)) {
			flowers.addFlower(flower);
		} else if (qName.equalsIgnoreCase(FlowerFields.ELEMENT_NAME)) {
			flower.setName(currentValue.toString());
		} else if (qName.equalsIgnoreCase(FlowerFields.ELEMENT_SOIL)) {
			flower.setSoil(currentValue.toString());
		} else if (qName.equalsIgnoreCase(FlowerFields.ELEMENT_ORIGIN)) {
			flower.setOrigin(currentValue.toString());
		} else if (qName.equalsIgnoreCase(VisualParametersFields.ELEMENT_STEM_COLOUR)) {
			flower.getVisualParameters().setStemColour(currentValue.toString());
		} else if (qName.equalsIgnoreCase(VisualParametersFields.ELEMENT_LEAF_COLOUR)) {
			flower.getVisualParameters().setLeafColour(currentValue.toString());
		} else if (qName.equalsIgnoreCase(AveLenFlowerFields.NODE_NAME)) {
			flower.getVisualParameters().getAveLenFlower().setValue(Integer.parseInt(currentValue.toString()));
		} else if (qName.equalsIgnoreCase(TemperatureFields.NODE_NAME)) {
			flower.getGrowingTips().getTemperature().setValue(Integer.parseInt(currentValue.toString()));
		} else if (qName.equalsIgnoreCase(WateringFields.NODE_NAME)) {
			flower.getGrowingTips().getWatering().setValue(Integer.parseInt(currentValue.toString()));
		} else if (qName.equalsIgnoreCase(FlowerFields.ELEMENT_MULTIPLYING)) {
			flower.setMultiplying(currentValue.toString());
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) {
		currentValue.append(ch, start, length);
	}
}