package com.epam.rd.java.basic.task8.parser.sax_parser;

import javax.xml.stream.XMLStreamException;

public interface CustomSAXParser<E> {
    void toXML(E e) throws XMLStreamException;
}
