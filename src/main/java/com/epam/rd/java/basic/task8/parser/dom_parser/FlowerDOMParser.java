package com.epam.rd.java.basic.task8.parser.dom_parser;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import com.epam.rd.java.basic.task8.parser.constants.FlowerFields;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class FlowerDOMParser {

    public static Flower parseFromXML(Element element) {
        String name = element.getElementsByTagName(FlowerFields.ELEMENT_NAME).item(0).getTextContent();
        String soil = element.getElementsByTagName(FlowerFields.ELEMENT_SOIL).item(0).getTextContent();
        String origin = element.getElementsByTagName(FlowerFields.ELEMENT_ORIGIN).item(0).getTextContent();
        String multiplying = element.getElementsByTagName(FlowerFields.ELEMENT_MULTIPLYING).item(0).getTextContent();

        VisualParameters visualParameters = VisualParametersDOMParser.parseFromXML(element);
        GrowingTips growingTips = GrowingTipsDOMParser.parseFromXML(element);

        return new Flower(name, soil, origin, visualParameters, growingTips, multiplying);
    }

    public static Element parseToXML(Document document, Flower flower) {
        Element flowerElement = document.createElement(FlowerFields.NODE_NAME);

        Element name = document.createElement(FlowerFields.ELEMENT_NAME);
        Element soil = document.createElement(FlowerFields.ELEMENT_SOIL);
        Element origin = document.createElement(FlowerFields.ELEMENT_ORIGIN);
        Element multiplying = document.createElement(FlowerFields.ELEMENT_MULTIPLYING);
        Element visualParameters = VisualParametersDOMParser
                .parseToXML(document, flower.getVisualParameters());
        Element growingTips = GrowingTipsDOMParser
                .parseToXML(document, flower.getGrowingTips());

        name.setTextContent(flower.getName());
        soil.setTextContent(flower.getSoil());
        origin.setTextContent(flower.getOrigin());
        multiplying.setTextContent(flower.getMultiplying());

        flowerElement.appendChild(name);
        flowerElement.appendChild(soil);
        flowerElement.appendChild(origin);
        flowerElement.appendChild(visualParameters);
        flowerElement.appendChild(growingTips);
        flowerElement.appendChild(multiplying);

        return flowerElement;
    }
}
