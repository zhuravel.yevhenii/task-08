package com.epam.rd.java.basic.task8.parser.stax_parser;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import com.epam.rd.java.basic.task8.parser.constants.FlowerFields;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;

public class FlowerStAXParser extends AbstractStAXParser implements StAXParser<Flower> {
    
    public FlowerStAXParser(XMLEventWriter writer, XMLEventFactory eventFactory) {
        super(writer, eventFactory);
    }

    @Override
    public void parseToXML(Flower flower) throws XMLStreamException {
        createStartElement(FlowerFields.NODE_NAME);
        createStartElement(FlowerFields.ELEMENT_NAME);
        createCharacters(flower.getName());
        createEndElement(FlowerFields.ELEMENT_NAME);
        createStartElement(FlowerFields.ELEMENT_SOIL);
        createCharacters(flower.getSoil());
        createEndElement(FlowerFields.ELEMENT_SOIL);
        createStartElement(FlowerFields.ELEMENT_ORIGIN);
        createCharacters(flower.getOrigin());
        createEndElement(FlowerFields.ELEMENT_ORIGIN);
        StAXParser<VisualParameters> visualParametersStAXParser = new VisualParametersStAXParser(writer, eventFactory);
        visualParametersStAXParser.parseToXML(flower.getVisualParameters());

        StAXParser<GrowingTips> growingTipsStAXParser = new GrowingTipsStAXParser(writer, eventFactory);
        growingTipsStAXParser.parseToXML(flower.getGrowingTips());
        createStartElement(FlowerFields.ELEMENT_MULTIPLYING);
        createCharacters(flower.getMultiplying());
        createEndElement(FlowerFields.ELEMENT_MULTIPLYING);
        createEndElement(FlowerFields.NODE_NAME);
    }
}
