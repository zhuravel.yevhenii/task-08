package com.epam.rd.java.basic.task8.exception;

public class XMLParserException extends Exception {

    public XMLParserException(String message, Throwable cause) {
        super(message, cause);
    }
}
