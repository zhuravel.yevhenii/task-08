package com.epam.rd.java.basic.task8.entity;

public class Watering {

    private String measure;
    private int value;

    public Watering() {
    }

    public Watering(String measure, int value) {
        this.measure = measure;
        this.value = value;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Watering{" +
                "measure='" + measure + "'" +
                ",\n value='" + value + "'" +
                '}';
    }
}
