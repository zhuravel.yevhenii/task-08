package com.epam.rd.java.basic.task8.parser.sax_parser;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.parser.constants.FlowersFields;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.util.Map;

public class FlowersSAXParser extends AbstractSAXParser implements CustomSAXParser<Flowers> {

    public FlowersSAXParser(XMLStreamWriter writer) {
        super(writer);
    }

    @Override
    public void toXML(Flowers flowers) throws XMLStreamException {
        writeStartElement(FlowersFields.NODE_NAME);

        for(Map.Entry<String, String> entry: flowers.getAllAttributes().entrySet()) {
            writeAttribute(entry.getKey(), entry.getValue());
        }

        CustomSAXParser<Flower> flowerSAXParser = new FlowerSAXParser(writer);
        for (Flower flower: flowers.getFlowers()) {
            flowerSAXParser.toXML(flower);
        }

        writeEndElement();
    }
}
