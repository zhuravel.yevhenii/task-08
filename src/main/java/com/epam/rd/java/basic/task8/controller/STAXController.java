package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.AveLenFlower;
import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.Temperature;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import com.epam.rd.java.basic.task8.entity.Watering;
import com.epam.rd.java.basic.task8.exception.XMLParserException;
import com.epam.rd.java.basic.task8.parser.constants.AveLenFlowerFields;
import com.epam.rd.java.basic.task8.parser.constants.FlowerFields;
import com.epam.rd.java.basic.task8.parser.constants.FlowersFields;
import com.epam.rd.java.basic.task8.parser.constants.GrowingTipsFields;
import com.epam.rd.java.basic.task8.parser.constants.TemperatureFields;
import com.epam.rd.java.basic.task8.parser.constants.VisualParametersFields;
import com.epam.rd.java.basic.task8.parser.constants.WateringFields;
import com.epam.rd.java.basic.task8.parser.stax_parser.FlowersStAXParser;
import com.epam.rd.java.basic.task8.parser.stax_parser.StAXParser;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler implements XMLController {

    private final String xmlFileName;
    private Flowers flowers;
    private Flower flower;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    @Override
    public Flowers parseFromXML() throws XMLParserException {
        flowers = new Flowers();

        try {
            XMLEventReader reader = XMLInputFactory.newFactory().createXMLEventReader(new FileInputStream(xmlFileName));

            while (reader.hasNext()) {
                XMLEvent event = reader.nextEvent();

                if (event.isStartElement()) {

                    StartElement element = event.asStartElement();
                    String qName = element.getName().getLocalPart();

                    if (qName.equalsIgnoreCase(FlowersFields.NODE_NAME)) {
                        element.getNamespaces().forEachRemaining(
                                namespace -> {
                                    String prefix = Stream.of(namespace.getName().getPrefix(), namespace.getPrefix())
                                            .filter(s -> !s.equals(""))
                                            .collect(Collectors.joining(":"));
                                    flowers.addAttribute(prefix, namespace.getValue());
                                });
                        element.getAttributes().forEachRemaining(
                                attribute -> {
                                    String prefix = Stream.of(attribute.getName().getPrefix(), attribute.getName().getLocalPart())
                                            .filter(s -> !s.equals(""))
                                            .collect(Collectors.joining(":"));
                                    flowers.addAttribute(prefix, attribute.getValue());
                                });
                    } else if (qName.equalsIgnoreCase(FlowerFields.NODE_NAME)) {
                        flower = new Flower();
                        flowers.addFlower(flower);
                    } else if (qName.equalsIgnoreCase(FlowerFields.ELEMENT_NAME)) {
                        event = reader.nextEvent();
                        flower.setName(event.asCharacters().getData());
                    } else if (qName.equalsIgnoreCase(FlowerFields.ELEMENT_SOIL)) {
                        event = reader.nextEvent();
                        flower.setSoil(event.asCharacters().getData());
                    } else if (qName.equalsIgnoreCase(FlowerFields.ELEMENT_ORIGIN)) {
                        event = reader.nextEvent();
                        flower.setOrigin(event.asCharacters().getData());

                    } else if (qName.equalsIgnoreCase(VisualParametersFields.NODE_NAME)) {
                        flower.setVisualParameters(new VisualParameters());
                    } else if (qName.equalsIgnoreCase(VisualParametersFields.ELEMENT_STEM_COLOUR)) {
                        event = reader.nextEvent();
                        flower.getVisualParameters().setStemColour(event.asCharacters().getData());
                    } else if (qName.equalsIgnoreCase(VisualParametersFields.ELEMENT_LEAF_COLOUR)) {
                        event = reader.nextEvent();
                        flower.getVisualParameters().setLeafColour(event.asCharacters().getData());
                    } else if (qName.equalsIgnoreCase(AveLenFlowerFields.NODE_NAME)) {
                        AveLenFlower aveLenFlower = new AveLenFlower();
                        aveLenFlower.setMeasure(element.getAttributeByName(new QName(AveLenFlowerFields.ATTR_MEASURE)).getValue());
                        event = reader.nextEvent();
                        aveLenFlower.setValue(Integer.parseInt(event.asCharacters().getData()));
                        flower.getVisualParameters().setAveLenFlower(aveLenFlower);

                    } else if (qName.equalsIgnoreCase(GrowingTipsFields.NODE_NAME)) {
                        flower.setGrowingTips(new GrowingTips());
                    } else if (qName.equalsIgnoreCase(TemperatureFields.NODE_NAME)) {
                        Temperature temperature = new Temperature();
                        temperature.setMeasure(element.getAttributeByName(new QName(TemperatureFields.ATTR_MEASURE)).getValue());
                        event = reader.nextEvent();
                        temperature.setValue(Integer.parseInt(event.asCharacters().getData()));
                        flower.getGrowingTips().setTemperature(temperature);
                    } else if (qName.equalsIgnoreCase(GrowingTipsFields.ELEMENT_LIGHTING)) {
                        flower.getGrowingTips().setLighting(element.getAttributeByName(
                                new QName(GrowingTipsFields.ATTR_LIGHT_REQUIRING)).getValue());
                    } else if (qName.equalsIgnoreCase(WateringFields.NODE_NAME)) {
                        Watering watering = new Watering();
                        watering.setMeasure(element.getAttributeByName(new QName(WateringFields.ATTR_MEASURE)).getValue());
                        event = reader.nextEvent();
                        watering.setValue(Integer.parseInt(event.asCharacters().getData()));
                        flower.getGrowingTips().setWatering(watering);

                    } else if (qName.equalsIgnoreCase(FlowerFields.ELEMENT_MULTIPLYING)) {
                        event = reader.nextEvent();
                        flower.setMultiplying(event.asCharacters().getData());
                    }
                }
            }
        } catch (XMLStreamException | FileNotFoundException e) {
            e.printStackTrace();
            throw new XMLParserException("SAtX parse from XML error", e);
        }
        return flowers;
    }

    @Override
    public void parseToXML(Flowers flowers, String outputXmlFile) throws XMLParserException {
        XMLOutputFactory factory = XMLOutputFactory.newInstance();
        XMLEventFactory eventFactory = XMLEventFactory.newInstance();
        try {
            XMLEventWriter writer = factory.createXMLEventWriter(new FileOutputStream(outputXmlFile));

            StAXParser<Flowers> parser = new FlowersStAXParser(writer, eventFactory);
            parser.parseToXML(flowers);

            writer.add(eventFactory.createEndDocument());
            writer.flush();
            writer.close();

        } catch (XMLStreamException | FileNotFoundException e) {
            e.printStackTrace();
            throw new XMLParserException("SAX parse to XML error", e);
        }
    }
}