package com.epam.rd.java.basic.task8.parser.sax_parser;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import com.epam.rd.java.basic.task8.parser.constants.FlowerFields;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

public class FlowerSAXParser extends AbstractSAXParser implements CustomSAXParser<Flower> {

    public FlowerSAXParser(XMLStreamWriter writer) {
        super(writer);
    }

    @Override
    public void toXML(Flower flower) throws XMLStreamException {
        writeStartElement(FlowerFields.NODE_NAME);
        writeStartElement(FlowerFields.ELEMENT_NAME);
        writeCharacters(flower.getName());
        writeEndElement();
        writeStartElement(FlowerFields.ELEMENT_SOIL);
        writeCharacters(flower.getSoil());
        writeEndElement();
        writeStartElement(FlowerFields.ELEMENT_ORIGIN);
        writeCharacters(flower.getOrigin());
        writeEndElement();
        CustomSAXParser<VisualParameters> visualParametersSAXParser = new VisualParametersSAXParser(writer);
        visualParametersSAXParser.toXML(flower.getVisualParameters());

        CustomSAXParser<GrowingTips> growingTipsSAXParser = new GrowingTipsSAXParser(writer);
        growingTipsSAXParser.toXML(flower.getGrowingTips());
        
        writeStartElement(FlowerFields.ELEMENT_MULTIPLYING);
        writeCharacters(flower.getMultiplying());
        writeEndElement();
        writeEndElement();
    }
}
